# tt: time tracker

Application to track time for multiple projects.

## Usage

`tt [OPTIONS]`

## Options:

| Option | Description |
| --- | --- |
| `-s`, `--start`/`--stop` | Start/Stop time recording. If a label is not provided, the time will be stored under the default label. |
| `-S`, `--summary` | Print the recorded times for a week. If the week number is not specified, the actual one will be used. By default, it will print the times for all labels, you can use '--label' to see the times for a specific label. |
| `-l=<name>`, `--label=<name>` | Specify the label to use with other options. (default: work) |
| `-w=<number>`, `--week=<number>` | ISO week number to use for summary. If not specified, the default is the actual ISO week number. You can use negative number to get previous weeks. |
| `-y=<number>`, `--year=<number>` | ISO year to use for summary. If not specified, the default is the actual ISO year. |
| `-L=<number>`, `--launch-time=<number>` | Time that will be added each day for the launch break. The value is in minutes. (default: 30 minutes) |
| `--get-week` | Get the current ISO week number. |
| `--data-dir` | Get the path to the directory containing the data files. |
| `--h`, `--help` | Print help |
| `-v`, `--version` | Print the version of the application |

## Build

Requirement:

- `nim >= 2.0.0`

To build the project:

`nimble build`

It will build in debug mode, some log will be enabled. To build a release version add `-d:release`.

## Install

```
git clone https://gitlab.com/Emplis/tt
cd tt
nimble install
```

Alternatively, you can do:

```
nimble install https://gitlab.com/Emplis/tt
```

## Uninstall

Simply run `nimble uninstall tt`.

The files used to store the data will not be deleted, you need to do it manually. To get the path of the directory used to store the files, run `tt --data-dir` before uninstalling `tt`.


## License

See [`LICENSE`](LICENSE)
