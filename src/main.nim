import std/strformat
import std/terminal
import std/paths
import std/times
import std/files
import std/syncio
import std/options
import std/tables
import std/json

import utils
import cli

type
    LogEntry = object
        label: string
        first_time: DateTime
        last_time: DateTime
        duration: int
        recording: bool

proc `$`(self: LogEntry): string =
    &"label: {self.label}, first_time: {self.first_time}, last_time: {self.last_time}, duration: {self.duration}, recording: {self.recording}"

proc getWeekLogs(year: IsoYear, week: IsoWeekRange, label: string = ""): seq[LogEntry] =
    let
        dataFilePath: Path = getDataFilePath(year)
    var
        file: File
        rawJsonData: string
        logEntries: seq[LogEntry] = newSeq[LogEntry]()

    if not fileExists(dataFilePath):
        return logEntries

    try:
        printDebug(&"Reading file: {dataFilePath.string}")
        file = open(dataFilePath.string, fmRead)
        rawJsonData = readAll(file)
    except IOError:
        file.close()
        quitWithError("Failed to read date file")

    file.close()

    try:
        let
            jsonData = parseJson(rawJsonData)

        if jsonData{$week} == nil:
            printDebug(&"No log entries for week {week}")
            return logEntries

        printDebug(&"getCompleteWeekLogs: number of label for week {week}: {len(jsonData[$week])}")
        for weekLabel in keys(jsonData[$week]):
            printDebug(&"getCompleteWeekLogs: number of entry for label {weekLabel}: {len(jsonData[$week][weekLabel])}")
            if label != "" and weekLabel != label:
                continue
            for dayNode in items(jsonData[$week][weekLabel]):
                let
                    first_time: DateTime = parse(dayNode["first_time"].getStr(), DATETIME_FORMAT)
                    last_time: DateTime = parse(dayNode["last_time"].getStr(), DATETIME_FORMAT)
                    duration: int = dayNode["duration"].getInt()
                    recording: bool = dayNode["recording"].getBool()

                logEntries.add(LogEntry(label: weekLabel, first_time: first_time, last_time: last_time, duration: duration, recording: recording))
    except JsonParsingError, KeyError:
        quitWithError("Malformed JSON file")

    return logEntries

proc getDayLogs(year: IsoYear, week: IsoWeekRange, day: WeekDay, label: string = DEFAULT_LABEL): Option[LogEntry] =
    let
        logEntries = getWeekLogs(year, week, label)

    if logEntries.len == 0:
        return none(LogEntry)

    for logEntry in logEntries:
        if logEntry.first_time.weekday == day:
            return some(logEntry)

    return none(LogEntry)

proc writeLogEntry(data: LogEntry) =
    var
        file: File
        dataToWrite: string
        rawJsonData: string
        jsonData: JsonNode
        fileOpen: bool
    let
        (isoWeek, isoYear) = getIsoWeekAndYear(data.first_time)
        weekDay: WeekDay = data.first_time.weekday
        dataFilePath: Path = getDataFilePath(isoYear)
        fileExist: bool = fileExists(dataFilePath)

    if fileExist and not open(file, dataFilePath.string, fmRead):
        quitWithError(&"Failed to open file '{dataFilePath.string}'")

    try:
        if not fileExist:
            var x = %*
                {
                    $isoWeek: {
                        data.label: [
                            {
                                "first_time": data.first_time.format(DATETIME_FORMAT),
                                "last_time": data.last_time.format(DATETIME_FORMAT),
                                "duration": data.duration,
                                "recording": data.recording
                            }
                        ]
                    }
                }

            dataToWrite = pretty(x)
        else:
            rawJsonData = readAll(file)
            jsonData = parseJson(rawJsonData)

            let updatedNode: JsonNode = %*
                {
                    "first_time": data.first_time.format(DATETIME_FORMAT),
                    "last_time": data.last_time.format(DATETIME_FORMAT),
                    "duration": data.duration,
                    "recording": data.recording
                }

            if jsonData{$isoWeek} != nil:
                if jsonData[$isoWeek]{data.label} != nil:
                    var dayIdx: int = -1
                    for i in 0..(len(jsonData[$isoWeek][data.label]) - 1):
                        if parse(jsonData[$isoWeek][data.label][i]["first_time"].getStr(), DATETIME_FORMAT).weekday == weekDay:
                            dayIdx = i
                    if dayIdx != -1:
                        printDebug(&"update the node: {updatedNode}")
                        jsonData[$isoWeek][data.label][dayIdx]["first_time"] = updatedNode["first_time"]
                        jsonData[$isoWeek][data.label][dayIdx]["last_time"] = updatedNode["last_time"]
                        jsonData[$isoWeek][data.label][dayIdx]["duration"] = updatedNode["duration"]
                        jsonData[$isoWeek][data.label][dayIdx]["recording"] = updatedNode["recording"]
                    else: # entries for this label has already been added in the week but not for this day
                        add(jsonData[$isoWeek][data.label], updatedNode)
                else: # entries for this week has already been added but not with this tag
                    jsonData[$isoWeek][data.label] = %* [ updatedNode ]
            else: # file exist but it's the first entry for this week
                jsonData[$isoWeek] = %* { data.label: [ updatedNode ]}

            printDebug(&"jsonData: {jsonData}")
            dataToWrite = pretty(jsonData)

        printDebug(&"dataToWrite: {dataToWrite}")
        printDebug(&"Writing file: {dataFilePath.string}")

        if fileExist:
            fileOpen = reopen(file, dataFilePath.string, fmWrite)
            if not fileOpen:
                close(file)
                quitWithError("Failed to reopen data file")
        else:
            fileOpen = open(file, dataFilePath.string, fmWrite)
            if not fileOpen:
                close(file)
                quitWithError("Failed to open data file")

        write(file, dataToWrite)
    except JsonParsingError, KeyError:
        close(file)
        quitWithError("Malformed JSON file")
    except IOError:
        close(file)
        quitWithError(&"Failed to write log entry: {getCurrentExceptionMsg()}")

    close(file)

proc doLogTime(label: string) =
    let
        datetime: DateTime = now()
        (isoWeek, isoYear) = getIsoWeekAndYear(datetime)
        prevLogEntry: Option[LogEntry] = getDayLogs(isoYear, isoWeek, datetime.weekday, label)
    var
        newLogEntry: LogEntry

    newLogEntry.label = label

    if prevLogEntry.isSome:
        printDebug(&"prev log entry: {prevLogEntry}")
        if prevLogEntry.get().recording:
            let
                secondsSinceLastEntry = inSeconds(datetime - prevLogEntry.get().last_time)
            newLogEntry.first_time = prevLogEntry.get().first_time
            newLogEntry.last_time = datetime
            newLogEntry.duration = prevLogEntry.get().duration + secondsSinceLastEntry
            newLogEntry.recording = false

            echo &"Stopping record for {label}, {formatTime(secondsSinceLastEntry)} since last entry (total today: {formatTime(newLogEntry.duration)})"
        else:
            newLogEntry.first_time = prevLogEntry.get().first_time
            newLogEntry.last_time = datetime
            newLogEntry.duration = prevLogEntry.get().duration
            newLogEntry.recording = true

            echo &"Re-starting record for {label}"
    else:
        newLogEntry.first_time = datetime
        newLogEntry.last_time = datetime
        newLogEntry.duration = 0
        newLogEntry.recording = true

        echo &"Starting record for {label}"

    writeLogEntry(newLogEntry)

proc showWeek(year: IsoYear, week: IsoWeekRange, label: string = "", launchTime: Duration = DEFAULT_LAUNCH_TIME) =
    let
        weekLogs: seq[LogEntry] = getWeekLogs(year, week, label)
        today: DateTime = now()
    var
        totalTimes = initTable[string, array[7, int]]() # nim 0-memory by default
        total: int = 0

    if len(weekLogs) == 0:
        echo &"No recorded times for week #{week} in {year}."
        return

    for logEntry in weekLogs:
        var dayIdx = weekDayIdx(logEntry.first_time.weekday)
        if logEntry.label notin totalTimes:
            totalTimes[logEntry.label] = [0, 0, 0, 0, 0, 0, 0]
        if logEntry.recording:
            if getDateStr(today) == getDateStr(logEntry.first_time):
                totalTimes[logEntry.label][dayIdx] = logEntry.duration + inSeconds(today - logEntry.last_time)
            else:
                printInfo(&"Entry for {logEntry.first_time.weekday} in '{logEntry.label}' hasn't been completed.")
        else:
            totalTimes[logEntry.label][dayIdx] = logEntry.duration

    for l in keys(totalTimes):
        stdout.styledWriteLine(styleUnderscore, &"{l}:")
        for i in 0..6:
            if totalTimes[l][i] == 0:
                continue

            echo &"\t{weekDayName(i)}: {formatTime(totalTimes[l][i] + launchTime.inSeconds())}"
            total += totalTimes[l][i] + launchTime.inSeconds()

    echo ""
    styledEcho styleUnderscore, "Total:", resetStyle, " ", styleBright, &"{formatTime(total)}"

proc printCurrentWeek() =
    let
        (isoWeek, isoYear) = getIsoWeekAndYear(now())

    echo &"Week #{isoWeek} of {isoYear}."

proc main() =
    let
        args: Args = getArgs()

    if args.help:
        printHelp()
    elif args.dataDir:
        printDataDirPath()
    elif args.version:
        printVersion()
    elif args.toggleRecord:
        if args.label == "":
            doLogTime(DEFAULT_LABEL)
        else:
            doLogTime(args.label)
    elif args.summry:
        showWeek(args.year, args.week, args.label, args.launchTime)
    elif args.getWeek:
        printCurrentWeek()

    quit(0)

when isMainModule:
    main()
