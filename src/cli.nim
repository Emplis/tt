import std/parseopt
import std/strformat
import std/strutils
import std/terminal
import std/times
import std/syncio

import utils

type
    Args* = object
        toggleRecord*: bool
        summry*: bool
        label*: string
        launchTime*: Duration
        week*: IsoWeekRange = 1 # the range doesn't include 0, so the default value must be set to 1
        year*: IsoYear
        getWeek*: bool
        dataDir*: bool
        version*: bool
        help*: bool

proc printHelp*() =
    echo "Application to track time for multiple projects."
    echo ""
    styledEcho styleBright, styleUnderscore, "Usage:", resetStyle, " ", styleBright, &"{getAppName()}", resetStyle, " [OPTIONS]"
    echo ""
    styledEcho styleBright, styleUnderscore, "Options:"
    echo ""
    styledEcho styleBright, "  -s, --start/--stop"
    echo "\tStart/Stop time recording. If a label is not provided, the time will be stored under the default label."
    echo ""
    styledEcho styleBright, "  -S, --summary"
    echo "\tPrint the recorded times for a week. If the week number is not specified, the actual one will be used. By default, it will print the times for all labels, you can use '--label' to see the times for a specific label."
    echo ""
    styledEcho styleBright, "  -l=<name>, --label=<name>"
    echo &"\tSpecify the label to use with other options. (default: {DEFAULT_LABEL})"
    echo ""
    styledEcho styleBright, "   -w=<number>, --week=<number>"
    echo &"\tISO week number to use for summary. If not specified, the default is the actual ISO week number."
    echo "\tYou can use negative number to get previous weeks."
    echo ""
    styledEcho styleBright, "  -y=<number>, --year=<number>"
    echo &"\tISO year to use for summary. If not specified, the default is the actual ISO year."
    echo ""
    styledEcho styleBright, "  -L=<number>, --launch-time=<number>"
    echo &"\tTime that will be added each day for the launch break. The value is in minutes. (default: {DEFAULT_LAUNCH_TIME})"
    echo ""
    styledEcho styleBright, "  --get-week"
    echo &"\tGet the current ISO week number."
    echo ""
    styledEcho styleBright, "  --data-dir"
    echo &"\tGet the path to the directory containing the data files."
    echo ""
    styledEcho styleBright, "  --h, --help"
    echo "\tPrint help"
    echo ""
    styledEcho styleBright, "  -v, --version"
    echo &"\tPrint the version of the application"

proc checkOptionValue(key: string, value: string) =
    if value == "":
        quitWithError(&"'{key}' need a value")

proc getArgs*(): Args =
    let
        (actualIsoWeek, actualIsoYear) = getIsoWeekAndYear(now())
    var
        p = initOptParser()
        args: Args
        yearSetByNegativeWeek: bool = false

    args.toggleRecord = false
    args.summry = false
    args.label = ""
    args.launchTime = DEFAULT_LAUNCH_TIME
    args.week = actualIsoWeek
    args.year = actualIsoYear
    args.getWeek = false
    args.dataDir = false
    args.version = false
    args.help = false

    while true:
        p.next()

        case p.kind
        of cmdEnd: break
        of cmdShortOption, cmdLongOption:
            printDebug(&"Passed option '{p.key}' with value '{p.val}'")
            if p.key == "start" or p.key == "stop" or p.key == "s":
                args.toggleRecord = true
            elif p.key == "summary" or p.key == "S":
                args.summry = true
            elif p.key == "label" or p.key == "l":
                checkOptionValue(p.key, p.val)
                args.label = p.val
            elif p.key == "week" or p.key == "w":
                checkOptionValue(p.key, p.val)
                try:
                    let week: int = parseInt(p.val)
                    if week < 0:
                        (args.week, args.year) = getIsoWeekAndYear(now() - initDuration(weeks=abs(week)))
                        yearSetByNegativeWeek = true
                    else:
                        args.week = IsoWeekRange(week)
                except RangeDefect:
                    quitWithError(&"'{p.key}' value must be in range [1, 53]")
                except ValueError:
                    quitWithError(&"'{p.key}' need a valid integer")
            elif p.key == "year" or p.key == "y":
                checkOptionValue(p.key, p.val)
                try:
                    let year: int = parseInt(p.val)
                    if year <= 0:
                        quitWithError(&"'{p.key}' value must be a strictly positive integer")
                    elif year > actualIsoYear.int:
                        quitWithError(&"'{p.key}' value can't be in the future")
                    args.year = IsoYear(year)
                except ValueError:
                    quitWithError(&"'{p.key}' need a valid integer")
            elif p.key == "launch-time" or p.key == "L":
                checkOptionValue(p.key, p.val)
                try:
                    let launchTime: int = parseInt(p.val)
                    if launchTime < 0:
                        quitWithError(&"'{p.key}' only take positive integer")
                    args.launchTime = initDuration(minutes=launchTime)
                except ValueError:
                    quitWithError(&"'{p.key}' need a valid integer")
            elif p.key == "get-week":
                args.getWeek = true
            elif p.key == "data-dir":
                args.dataDir = true
            elif p.key == "version" or p.key == "v":
                args.version = true
            elif p.key == "help" or p.key == "h":
                args.help = true
            else:
                printHelp()
                quitWithError(&"Invalid option '{p.key}'")
        of cmdArgument:
            printHelp()
            quitWithError(&"Invalid argument '{p.key}'")

    if args.year == actualIsoYear and args.week > actualIsoWeek:
        quitWithError("The week can't be in the future")

    return args
