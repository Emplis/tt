import std/strformat
import std/terminal
import std/paths
import std/appdirs
import std/times
import std/syncio
import std/dirs
import std/os

const
    VERSION: string = "1.0.4"
    DEFAULT_LABEL*: string = "work"
    DEFAULT_LAUNCH_TIME*: Duration = initDuration(minutes=30)
    DATETIME_FORMAT*: string = "yyyy-MM-dd'T'HH:mm:sszzz"

proc getAppName*(): string =
    return splitPath(getAppFilename()).tail

proc printVersion*() =
    echo &"{getAppName()} version: {VERSION}"
    echo &"Nim version: {NimVersion}"

proc printInfo*(msg: string) =
    echo &"INFO: {msg}"

proc printError*(msg: string) =
    stderr.styledWriteLine(fgRed, &"ERROR: {msg}")
    flushFile(stderr)

proc printDebug*(msg: string) =
    when not defined(release):
        stdout.writeLine(&"DEBUG: {msg}")
        flushFile(stdout)

proc quitWithError*(msg: string, err: int = 1) =
    printError(msg)
    quit(err)

proc getDataDirPath(): Path =
    appdirs.getDataDir() / Path(getAppName())

proc getDataFilePath*(year: IsoYear): Path =
    var
        dataFilePath: Path
    let
        dataPath: Path = getDataDirPath()

    if not dirExists(dataPath):
        try:
            printInfo(&"Creating data folder: {dataPath.string}")
            createDir(dataPath)
        except IOError:
            quitWithError(&"Failed to create data folder: {dataPath.string}")

    dataFilePath = dataPath / Path(&"{year}.json")

    return dataFilePath

proc printDataDirPath*() =
    let
        dataDirPath: Path = absolutePath(getDataDirPath())

    if dirExists(dataDirPath):
        if isatty(stdout):
            echo &"\e]8;;file://{dataDirPath.string}\e\\{dataDirPath.string}\e]8;;\e\\"
        else:
            echo &"{dataDirPath.string}"
    else:
        echo "The data directory does not exist."

proc weekDayIdx*(weekDay: WeekDay): int =
    case $weekDay
    of "Monday": return 0
    of "Tuesday": return 1
    of "Wednesday": return 2
    of "Thursday": return 3
    of "Friday": return 4
    of "Saturday": return 5
    of "Sunday": return 6

proc weekDayName*(weekDayIdx: int): string =
    case weekDayIdx
    of 0: return "Monday"
    of 1: return "Tuesday"
    of 2: return "Wednesday"
    of 3: return "Thursday"
    of 4: return "Friday"
    of 5: return "Saturday"
    of 6: return "Sunday"
    else: return ""

proc formatTime*(seconds: int): string =
    let
        secondUnit = "s"
        minuteUnit = "m"
        hourUnit = "h"
        separator = ""
    var
        hours: int
        minutes: int

    if seconds < 60:
        return &"{seconds}{secondUnit}"
    elif seconds < 3600:
        minutes = seconds div 60
        return &"{minutes}{minuteUnit}{separator}{seconds mod 60}{secondUnit}"
    else:
        hours = seconds div 3600
        let remaining_sec: int = seconds mod 3600
        minutes = remaining_sec div 60
        return &"{hours}{hourUnit}{separator}{minutes}{minuteUnit}{separator}{remaining_sec mod 60}{secondUnit}"
