version     = "1.0.4"
author      = "Théo Battrel"
description = "Simple time tracking application"
license     = "MIT"

requires "nim >= 2.0.0"

srcDir = "src"
binDir = "build"
namedBin["main"] = "tt"